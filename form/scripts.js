function myFunction(){
    var bday = document.getElementById("bday").value;
    var agediff = new Date() - new Date(bday);
    var agenow = Math.floor(agediff/1000/60/60/24/365);
    var under13 = document.getElementById("DOB-13");
    var over50 = document.getElementById("DOB-50");
    if (agenow < 13) {
        under13.innerHTML = "WARNING: Too Young to Register";
        under13.style.display = "inline-block";
        over50.style.display = "none";
    } 
    else if (agenow > 50) {
        over50.innerHTML = "<a href='helplink.html'>" + "Click Here For Assistance" + "</a>";
        over50.style.display = "inline-block";
        under13.style.display = "none";
    }
  }